package com.example.connectingdatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class AllSchools extends ListActivity implements OnItemClickListener {
	private ProgressDialog pDialog;

	JSONParser jParser = new JSONParser();

	ArrayList<HashMap<String, String>> schoolList;

	private static String url_pull_average = "lamp.cse.fau.edu/~jahring1/greenwisekids/pullaveragescores.php";

	private static final String TAG_SUCCESS = "success";
	private static final String TAG_ITEM = "item";
	private static final String TAG_SCHOOL_ID = "school_id";
	private static final String TAG_SCHOOL_STRING = "school_string";
	private static final String TAG_ENERGY = "energy";
	private static final String TAG_TRANSPORTATION = "transportation";
	private static final String TAG_WATER = "water";
	private static final String TAG_RECYCLE = "recycle";
	private static final String TAG_TOTAL = "total";

	JSONArray items = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.all_schools);

		schoolList = new ArrayList<HashMap<String, String>>();

		new LoadAllSchools().execute();

		ListView lv = getListView();
		lv.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		//List item variables
		String li_school_id = ((TextView) view.findViewById(R.id.school_id))
				.getText().toString();
		String li_school_string = ((TextView) view
				.findViewById(R.id.school_name)).getText().toString();
		String li_energy = ((TextView) view
				.findViewById(R.id.energy)).getText().toString();
		String li_water = ((TextView) view
				.findViewById(R.id.water)).getText().toString();
		String li_transportation = ((TextView) view
				.findViewById(R.id.transportation)).getText().toString();
		String li_recycle = ((TextView) view
				.findViewById(R.id.recycle)).getText().toString();
		String li_total = ((TextView) view
				.findViewById(R.id.total)).getText().toString();

		
		Intent i = new Intent(this, ViewSingleSchool.class);
		i.putExtra(TAG_SCHOOL_ID, Integer.parseInt(li_school_id));
		i.putExtra(TAG_SCHOOL_STRING, li_school_string);
		i.putExtra(TAG_ENERGY, Integer.parseInt(li_energy));
		i.putExtra(TAG_WATER, Integer.parseInt(li_water));
		i.putExtra(TAG_TRANSPORTATION, Integer.parseInt(li_transportation));
		i.putExtra(TAG_RECYCLE, Integer.parseInt(li_recycle));
		i.putExtra(TAG_TOTAL, Integer.parseInt(li_total));
		startActivity(i);

	}

	class LoadAllSchools extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AllSchools.this);
			pDialog.setMessage("Loading Schools. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			JSONObject json = jParser.makeHttpRequest(url_pull_average, "GET",
					params);
			Log.d("All schools: ", json.toString());
			try {
				int success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					items = json.getJSONArray(TAG_ITEM);
					for (int i = 0; i < items.length(); i++) {
						JSONObject c = items.getJSONObject(i);

						int id = Integer.parseInt(c.getString(TAG_SCHOOL_ID));
						String name = c.getString(TAG_SCHOOL_STRING);
						int energy = c.getInt(TAG_ENERGY);
						int transportation = c.getInt(TAG_TRANSPORTATION);
						int recycle = c.getInt(TAG_RECYCLE);
						int water = c.getInt(TAG_WATER);
						int total = c.getInt(TAG_TOTAL);


						HashMap<String, String> map = new HashMap<String, String>();
						map.put(TAG_SCHOOL_ID, String.valueOf(id));
						map.put(TAG_SCHOOL_STRING, name);
						map.put(TAG_ENERGY, String.valueOf(energy));
						map.put(TAG_TRANSPORTATION,
								String.valueOf(transportation));
						map.put(TAG_WATER, String.valueOf(water));
						map.put(TAG_RECYCLE, String.valueOf(recycle));
						map.put(TAG_TOTAL, String.valueOf(total));

						schoolList.add(map);
					}
				} else {
					// No schools found. Don't do anything.
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			runOnUiThread(new Runnable() {
				public void run() {
					ListAdapter adapter = new SimpleAdapter(AllSchools.this,
							schoolList, R.layout.list_school_item,
							new String[] { TAG_SCHOOL_ID, TAG_SCHOOL_STRING,
									TAG_ENERGY, TAG_TRANSPORTATION, TAG_WATER,
									TAG_RECYCLE, TAG_TOTAL }, new int[] {
									R.id.school_id, R.id.school_name,
									R.id.energy, R.id.transportation,
									R.id.water, R.id.recycle, R.id.total });
					setListAdapter(adapter);
				}
			});
		}
	}

}
