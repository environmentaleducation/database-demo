package com.example.connectingdatabase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class ViewSingleSchool extends Activity {
	private static final String TAG_SCHOOL_ID = "school_id";
	private static final String TAG_SCHOOL_STRING = "school_string";
	private static final String TAG_ENERGY = "energy";
	private static final String TAG_TRANSPORTATION = "transportation";
	private static final String TAG_WATER = "water";
	private static final String TAG_RECYCLE = "recycle";
	private static final String TAG_TOTAL = "total";
	
	TextView tvSchoolID;
	TextView tvSchoolString;
	TextView tvEnergy;
	TextView tvTransportation;
	TextView tvWater;
	TextView tvRecycle;
	TextView tvTotal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_single_school);
		
		Intent i = getIntent();
		int id = i.getIntExtra(TAG_SCHOOL_ID, -1);
		String school_string= i.getStringExtra(TAG_SCHOOL_STRING);
		int energy = i.getIntExtra(TAG_ENERGY, -1);
		int transportation = i.getIntExtra(TAG_TRANSPORTATION, -1);
		int water = i.getIntExtra(TAG_WATER, -1);
		int recycle = i.getIntExtra(TAG_RECYCLE, -1);
		int total = i.getIntExtra(TAG_TOTAL, -1);
		
		tvSchoolID = (TextView)findViewById(R.id.value_average_school_id);
		tvSchoolString = (TextView)findViewById(R.id.value_average_school_string);
		tvEnergy = (TextView)findViewById(R.id.value_average_energy);
		tvTransportation = (TextView)findViewById(R.id.value_average_transportation);
		tvWater = (TextView)findViewById(R.id.value_average_water);
		tvRecycle = (TextView)findViewById(R.id.value_average_recycle);
		tvTotal = (TextView)findViewById(R.id.value_average_total);
		
		tvSchoolID.setText(String.valueOf(id));
		tvSchoolString.setText(school_string);
		tvEnergy.setText(String.valueOf(energy));
		tvTransportation.setText(String.valueOf(transportation));
		tvWater.setText(String.valueOf(water));
		tvRecycle.setText(String.valueOf(recycle));
		tvTotal.setText(String.valueOf(total));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_single_school, menu);
		return true;
	}

}
