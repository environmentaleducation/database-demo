package com.example.connectingdatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class ViewSingleKid extends Activity {

	private static int id = 0;
	private ProgressDialog pDialog;
	TextView tv_kid_id;
	TextView tv_kid_energy;
	TextView tv_kid_transportation;
	TextView tv_kid_water;
	TextView tv_kid_recycle;
	TextView tv_kid_total;

	JSONParser jParser = new JSONParser();

	ArrayList<HashMap<String, String>> kid;

	private static String url_pull_kid = "lamp.cse.fau.edu/~jahring1/greenwisekids/pullkidscore.php";

	private static final String TAG_SUCCESS = "success";
	private static final String TAG_ITEM = "item";
	private static final String TAG_ENERGY = "energy";
	private static final String TAG_TRANSPORTATION = "transportation";
	private static final String TAG_WATER = "water";
	private static final String TAG_RECYCLE = "recycle";
	private static final String TAG_TOTAL = "total_score";

	JSONArray items = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_single_kid);
		kid = new ArrayList<HashMap<String, String>>();
		Intent intent = getIntent();
		id = Integer.parseInt(intent
				.getStringExtra(UpdateDatabase.EXTRA_KID_ID));
		tv_kid_id = (TextView) findViewById(R.id.kid_score_id);
		tv_kid_energy = (TextView) findViewById(R.id.kid_score_energy);
		tv_kid_transportation = (TextView) findViewById(R.id.kid_score_transportation);
		tv_kid_water = (TextView) findViewById(R.id.kid_score_water);
		tv_kid_recycle = (TextView) findViewById(R.id.kid_score_recycle);
		tv_kid_total = (TextView) findViewById(R.id.kid_score_total);

		tv_kid_id.setText(String.valueOf(id));
		new LoadSingleKid().execute();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_single_kid, menu);
		return true;
	}

	class LoadSingleKid extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ViewSingleKid.this);
			pDialog.setMessage("Loading Kid information. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("kidID", String.valueOf(id)));
			JSONObject json = jParser.makeHttpRequest(url_pull_kid, "GET",
					params);
			Log.d("Kid: ", json.toString());
			try {
				int success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					items = json.getJSONArray(TAG_ITEM);
					JSONObject c = items.getJSONObject(0);
					int energy = c.getInt(TAG_ENERGY);
					int recycle = c.getInt(TAG_RECYCLE);
					int transportation = c.getInt(TAG_TRANSPORTATION);
					int water = c.getInt(TAG_WATER);
					int total = c.getInt(TAG_TOTAL);

					HashMap<String, String> map = new HashMap<String, String>();
					map.put(TAG_ENERGY, String.valueOf(energy));
					map.put(TAG_RECYCLE, String.valueOf(recycle));
					map.put(TAG_TRANSPORTATION, String.valueOf(transportation));
					map.put(TAG_WATER, String.valueOf(water));
					map.put(TAG_TOTAL, String.valueOf(total));

					kid.add(map);

				} else {
					// Kid with this id does not exist
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			tv_kid_energy.setText(kid.get(0).get(TAG_ENERGY));
			tv_kid_transportation.setText(kid.get(0).get(TAG_TRANSPORTATION));
			tv_kid_water.setText(kid.get(0).get(TAG_WATER));
			tv_kid_recycle.setText(kid.get(0).get(TAG_RECYCLE));
			tv_kid_total.setText(kid.get(0).get(TAG_TOTAL));
		}

	}

}
